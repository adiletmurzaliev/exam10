<?php

namespace App\Form;

use App\Entity\Rating;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RatingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quality', ChoiceType::class, [
                'label' => 'Качество статьи',
                'choices' => [
                    'Да' => Rating::POSITIVE,
                    'Нет' => Rating::NEGATIVE
                ]
            ])
            ->add('relevance', ChoiceType::class, [
                'label' => 'Актуальность',
                'choices' => [
                    'Да' => Rating::POSITIVE,
                    'Нет' => Rating::NEGATIVE
                ]
            ])
            ->add('satisfaction', ChoiceType::class, [
                'label' => 'Доволны?',
                'choices' => [
                    'Да' => Rating::POSITIVE,
                    'Нет' => Rating::NEGATIVE
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Оценить'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rating::class,
        ]);
    }
}
