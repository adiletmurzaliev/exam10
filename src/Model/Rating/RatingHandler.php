<?php

namespace App\Model\Rating;


use App\Entity\User;
use App\Repository\NewsRepository;
use App\Repository\RatingRepository;

class RatingHandler
{
    /**
     * @var RatingRepository
     */
    private $ratingRepository;
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(RatingRepository $ratingRepository, NewsRepository $newsRepository)
    {
        $this->ratingRepository = $ratingRepository;
        $this->newsRepository = $newsRepository;
    }

    public function calculateRatingForAllUsers()
    {
        $rating = $this->ratingRepository->getRatingForAllUsers();
        $newsCount = $this->newsRepository->getNewsCountAllUsers();

        dump($rating);
        dump($newsCount);

        $data = [];

        foreach ($newsCount as $news) {
            foreach ($rating as $rate) {
                if ($news['user_id'] === $rate['user_id']) {
                    $news = $news + $rate;
                }
            }
            $data[] = $news;
        }

        $result = [];

        foreach ($data as $user) {
            $result[$user['user_id']] = ($user['news'] + ($user['quality'] ?? 0) + ($user['relevance'] ?? 0)) / 3;
        }

        return $result;
    }

    public function calculateRating(User $user)
    {
        $rating = $this->ratingRepository->getRatingForUser($user);
        $newsCount = $this->newsRepository->getNewsCountUser($user);

        return (($newsCount['news'] ?? 0) + ($rating['quality'] ?? 0) + ($rating['relevance'] ?? 0)) / 3;
    }
}