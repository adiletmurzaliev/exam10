<?php

namespace App\Repository;

use App\Entity\News;
use App\Entity\Rating;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rating[]    findAll()
 * @method Rating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rating::class);
    }

    public function findRateByUserAndNews(User $user, News $news)
    {
        try {
            return $this->createQueryBuilder('r')
                ->andWhere('r.user = :user')
                ->setParameter('user', $user)
                ->andWhere('r.news = :news')
                ->setParameter('news', $news)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getRatingForAllUsers()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb->select('u.id AS user_id')
            ->from('App:Rating', 'r')
            ->join('r.news', 'n')
            ->join('n.author', 'u')
            ->addSelect('SUM(r.quality) AS quality')
            ->addSelect('SUM(r.relevance) AS relevance')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }

    public function getRatingForUser(User $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        try {
            return $qb->select('u.id AS user_id')
                ->from('App:Rating', 'r')
                ->join('r.news', 'n')
                ->join('n.author', 'u')
                ->andWhere('u.id = :user')
                ->setParameter('user', $user)
                ->addSelect('SUM(r.quality) AS quality')
                ->addSelect('SUM(r.relevance) AS relevance')
                ->groupBy('u.id')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

//    /**
//     * @return Rating[] Returns an array of Rating objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rating
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
