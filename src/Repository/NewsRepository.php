<?php

namespace App\Repository;

use App\Entity\News;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function getAdminPaginationDql()
    {
        return $this->createQueryBuilder('n')
            ->orderBy('n.createdAt', 'desc')
            ->getQuery();
    }

    public function getUserPaginationDql(User $author)
    {
        return $this->createQueryBuilder('n')
            ->where('n.author = :author')
            ->setParameter('author', $author)
            ->orderBy('n.createdAt', 'desc')
            ->getQuery();
    }

    public function getAllPaginationDql(?array $filter)
    {
        $dql = $this->createQueryBuilder('n')
            ->andWhere('n.publishedAt < :date')
            ->setParameter('date', new \DateTime())
            ->andWhere('n.active = :active')
            ->setParameter('active', true)
            ->orderBy('n.publishedAt', 'desc');

        if ($filter) {
            if (isset($filter['categories'])) {
                $dql->join('n.categories', 'c')
                    ->andWhere($dql->expr()->in('c.id', $filter['categories']));
            }

            if (isset($filter['tags'])) {
                $dql->join('n.tags', 't')
                    ->andWhere($dql->expr()->in('t.id', $filter['tags']));
            }
        }

        return $dql->andWhere('n.active = true')
            ->orderBy('n.publishedAt', 'desc')
            ->getQuery();
    }

    public function getNewsCountAllUsers()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb->select('u.id AS user_id')
            ->from('App:User', 'u')
            ->join('u.news', 'n')
            ->addSelect('COUNT(n.id) AS news')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }

    public function getNewsCountUser(User $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        try {
            return $qb->select('u.id AS user_id')
                ->from('App:User', 'u')
                ->join('u.news', 'n')
                ->andWhere('u.id = :user')
                ->setParameter('user', $user)
                ->addSelect('COUNT(n.id) AS news')
                ->groupBy('u.id')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
