<?php

namespace App\Controller\Profile;

use App\Entity\User;
use App\Model\Rating\RatingHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 *
 * Class MainController
 * @package App\Controller\Profile
 */
class MainController extends Controller
{
    /**
     * @Route("/", name="profile_main_index")
     *
     * @param RatingHandler $ratingHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(RatingHandler $ratingHandler)
    {
        /** @var User $user */
        $user = $this->getUser();

        $rating = $ratingHandler->calculateRating($user);

        return $this->render('profile/main/index.html.twig', [
            'user' => $user,
            'rating' => $rating
        ]);
    }
}