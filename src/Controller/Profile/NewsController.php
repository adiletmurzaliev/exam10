<?php

namespace App\Controller\Profile;

use App\Entity\News;
use App\Form\UserNewsType;
use App\Repository\NewsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile/news")
 *
 * Class NewsController
 * @package App\Controller\Profile
 */
class NewsController extends Controller
{
    /**
     * @Route("/", name="profile_news_index")
     *
     * @param Request $request
     * @param NewsRepository $newsRepository
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, NewsRepository $newsRepository, PaginatorInterface $paginator)
    {
        $query = $newsRepository->getUserPaginationDql($this->getUser());

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            3
        );

        return $this->render('profile/news/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/create", name="profile_news_create")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request, ObjectManager $manager)
    {
        $news = new News();

        $form = $this->createForm(UserNewsType::class, $news);
        $form->add('submit', SubmitType::class, ['label' => 'Добавить новость']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setAuthor($this->getUser());
            $manager->persist($news);
            $manager->flush();

            return $this->redirectToRoute('profile_news_index');
        }

        return $this->render('profile/news/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="profile_news_edit")
     *
     * @param Request $request
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, News $news, ObjectManager $manager)
    {
        if ($news->getAuthor() !== $this->getUser()) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(UserNewsType::class, $news);
        $form->add('submit', SubmitType::class, ['label' => 'Редактировать новость']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($news);
            $manager->flush();

            return $this->redirectToRoute('profile_news_index');
        }

        return $this->render('profile/news/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="profile_news_delete")
     *
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(News $news, ObjectManager $manager)
    {
        if ($news->getAuthor() !== $this->getUser()) {
            throw new NotFoundHttpException();
        }

        $manager->remove($news);
        $manager->flush();

        return $this->redirectToRoute('profile_news_index');
    }
}