<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\News;
use App\Entity\Rating;
use App\Form\CommentType;
use App\Form\RatingType;
use App\Repository\CommentRepository;
use App\Repository\RatingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends Controller
{
    /**
     * @Route("/show/{id}", name="main_show")
     *
     * @param News $news
     * @param CommentRepository $commentRepository
     * @param RatingRepository $ratingRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(
        News $news,
        CommentRepository $commentRepository,
        RatingRepository $ratingRepository
    )
    {
        $hideRatingForm = false;

        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment, [
            'action' => $this->generateUrl('main_comment', ['id' => $news->getId()])
        ]);

        $rating = new Rating();
        $ratingForm = $this->createForm(RatingType::class, $rating, [
            'action' => $this->generateUrl('main_rating', ['id' => $news->getId()])
        ]);

        if ($this->getUser() && $ratingRepository->findRateByUserAndNews($this->getUser(), $news)) {
            $hideRatingForm = true;
        }

        $comments = $commentRepository->getAllByNews($news);

        return $this->render('news/show.html.twig', [
            'news' => $news,
            'comments' => $comments,
            'commentForm' => $commentForm->createView(),
            'ratingForm' => $ratingForm->createView(),
            'hideRatingForm' => $hideRatingForm
        ]);
    }

    /**
     * @Route("/comment/{id}", name="main_comment")
     *
     * @param Request $request
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function commentAction(
        Request $request,
        News $news,
        ObjectManager $manager
    )
    {
        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment
                ->setUser($this->getUser())
                ->setNews($news);
            $manager->persist($comment);
            $manager->flush();
        }

        return $this->redirectToRoute('main_show', ['id' => $news->getId()]);
    }

    /**
     * @Route("/rating/{id}", name="main_rating")
     *
     * @param Request $request
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function ratingAction(Request $request, News $news, ObjectManager $manager)
    {
        $rating = new Rating();

        $form = $this->createForm(RatingType::class, $rating);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rating
                ->setUser($this->getUser())
                ->setNews($news);
            $manager->persist($rating);
            $manager->flush();
        }

        return $this->redirectToRoute('main_show', ['id' => $news->getId()]);
    }
}