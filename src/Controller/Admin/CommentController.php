<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/comment")
 *
 * Class CommentController
 * @package App\Controller\Admin
 */
class CommentController extends Controller
{
    /**
     * @Route("/", name="admin_comment_index")
     *
     * @param Request $request
     * @param CommentRepository $commentRepository
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, CommentRepository $commentRepository, PaginatorInterface $paginator)
    {
        $query = $commentRepository->getAdminPaginationDql();

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            3
        );

        return $this->render('admin/comment/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/publish/{id}", name="admin_comment_publish")
     *
     * @param Comment $comment
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function publishAction(Comment $comment, ObjectManager $manager)
    {
        if (!$comment->getActive()) {
            $comment->setActive(true);
            $manager->persist($comment);
            $manager->flush();
        }

        return $this->redirectToRoute('admin_comment_index');
    }

    /**
     * @Route("/delete/{id}", name="admin_comment_delete")
     *
     * @param Comment $comment
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Comment $comment, ObjectManager $manager)
    {
        $manager->remove($comment);
        $manager->flush();

        return $this->redirectToRoute('admin_comment_index');
    }
}