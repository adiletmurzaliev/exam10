<?php

namespace App\Controller\Admin;

use App\Repository\NewsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 *
 * Class MainController
 * @package App\Controller\Admin
 */
class MainController extends Controller
{
    /**
     * @Route("/", name="admin_main_index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        return $this->render('admin/main/index.html.twig', [

        ]);
    }
}