<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Form\AdminNewsType;
use App\Repository\NewsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/news")
 *
 * Class NewsController
 * @package App\Controller\Admin
 */
class NewsController extends Controller
{
    /**
     * @Route("/", name="admin_news_index")
     *
     * @param Request $request
     * @param NewsRepository $newsRepository
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, NewsRepository $newsRepository, PaginatorInterface $paginator)
    {
        $query = $newsRepository->getAdminPaginationDql();

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            3
        );

        return $this->render('admin/news/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/create", name="admin_news_create")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request, ObjectManager $manager)
    {
        $news = new News();

        $form = $this->createForm(AdminNewsType::class, $news);
        $form->add('submit', SubmitType::class, ['label' => 'Добавить новость']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setAuthor($this->getUser());
            if ($news->getActive()) {
                if (!$news->getPublishedAt()) {
                    $news->setPublishedAt(new \DateTime());
                }
            } else {
                $news->setPublishedAt(null);
            }
            $manager->persist($news);
            $manager->flush();

            return $this->redirectToRoute('admin_news_index');
        }

        return $this->render('admin/news/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_news_edit")
     *
     * @param Request $request
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, News $news, ObjectManager $manager)
    {
        $form = $this->createForm(AdminNewsType::class, $news);
        $form->add('submit', SubmitType::class, ['label' => 'Редактировать новость']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($news->getActive()) {
                if (!$news->getPublishedAt()) {
                    $news->setPublishedAt(new \DateTime());
                }
            } else {
                $news->setPublishedAt(null);
            }
            $manager->persist($news);
            $manager->flush();

            return $this->redirectToRoute('admin_news_index');
        }

        return $this->render('admin/news/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_news_delete")
     *
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(News $news, ObjectManager $manager)
    {
        $manager->remove($news);
        $manager->flush();

        return $this->redirectToRoute('admin_news_index');
    }
}