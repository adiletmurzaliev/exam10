<?php

namespace App\Controller;

use App\Model\Rating\RatingHandler;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use App\Repository\RatingRepository;
use App\Repository\TagRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @Route("/", name="main_index")
     *
     * @param Request $request
     * @param NewsRepository $newsRepository
     * @param PaginatorInterface $paginator
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param RatingRepository $ratingRepository
     * @param RatingHandler $ratingHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(
        Request $request,
        NewsRepository $newsRepository,
        PaginatorInterface $paginator,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository,
        RatingHandler $ratingHandler
    )
    {
        $filter = $this->get('session')->get('filter');

        $query = $newsRepository->getAllPaginationDql($filter);

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            2
        );

        $rating = $ratingHandler->calculateRatingForAllUsers();

        $categories = $categoryRepository->findAll();
        $tags = $tagRepository->findAll();

        return $this->render('main/index.html.twig', [
            'pagination' => $pagination,
            'categories' => $categories,
            'tags' => $tags,
            'rating' => $rating
        ]);
    }

    /**
     * @Route("/filter", name="main_filter")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function filterAction(Request $request)
    {
        $session = $this->get('session');
        $filter = [];

        if (!empty($request->request->get('categories'))) {
            $filter['categories'] = $request->request->get('categories');
        }

        if (!empty($request->request->get('tags'))) {
            $filter['tags'] = $request->request->get('tags');
        }

        $session->set('filter', $filter);

        return $this->redirectToRoute('main_index');
    }
}
