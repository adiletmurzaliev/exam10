<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    const ADMIN = 'admin';
    const USER_ONE = 'user1';
    const USER_TWO = 'user2';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin
            ->setUsername(self::ADMIN)
            ->setEnabled(true)
            ->setEmail('admin@mail.ru')
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        $user1 = new User();
        $user1
            ->setUsername(self::USER_ONE)
            ->setEnabled(true)
            ->setEmail('user1@mail.ru')
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user1);

        $user2 = new User();
        $user2
            ->setUsername(self::USER_TWO)
            ->setEnabled(true)
            ->setEmail('user2@mail.ru')
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user2);

        $manager->flush();

        $this->addReference(self::ADMIN, $admin);
        $this->addReference(self::USER_ONE, $user1);
        $this->addReference(self::USER_TWO, $user2);
    }
}