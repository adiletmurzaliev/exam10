<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $news = new News();
        $news
            ->setTitle('Новость о теннисе')
            ->setContent('Что-то про теннис')
            ->addCategory($this->getReference('Спорт'))
            ->addTag($this->getReference('Теннис'))
            ->setAuthor($this->getReference('user1'));
        $manager->persist($news);

        $news = new News();
        $news
            ->setTitle('Новость о футболе')
            ->setContent('Что-то про футбол')
            ->addCategory($this->getReference('Спорт'))
            ->addTag($this->getReference('Футбол'))
            ->setAuthor($this->getReference('user1'))
            ->setCreatedAt(new \DateTime('-7 days'))
            ->setPublishedAt(new \DateTime())
            ->setActive(true);
        $manager->persist($news);
        $this->addReference('football', $news);

        $news = new News();
        $news
            ->setTitle('Новость о теннисе2')
            ->setContent('Что-то про теннис')
            ->addCategory($this->getReference('Спорт'))
            ->addTag($this->getReference('Теннис'))
            ->addTag($this->getReference('Россия'))
            ->setAuthor($this->getReference('user1'))
            ->setPublishedAt(new \DateTime())
            ->setActive(true);
        $manager->persist($news);

        $news = new News();
        $news
            ->setTitle('Новость о Путине')
            ->setContent('Что-то про Путина и экономику')
            ->addCategory($this->getReference('Политика'))
            ->addCategory($this->getReference('Экономика'))
            ->addTag($this->getReference('Путин'))
            ->addTag($this->getReference('Россия'))
            ->setAuthor($this->getReference('user2'))
            ->setPublishedAt(new \DateTime())
            ->setActive(true);
        $manager->persist($news);
        $this->addReference('putin-news', $news);

        $news = new News();
        $news
            ->setTitle('Новость об играх')
            ->setContent('Что-то про игры')
            ->addCategory($this->getReference('Игры'))
            ->setAuthor($this->getReference('user2'));
        $manager->persist($news);

        $news = new News();
        $news
            ->setTitle('Новость об играх 2')
            ->setContent('Что-то про игры')
            ->addCategory($this->getReference('Игры'))
            ->setAuthor($this->getReference('user2'))
            ->setPublishedAt(new \DateTime())
            ->setActive(true);
        $manager->persist($news);

        $manager->flush();

        $news = new News();
        $news
            ->setTitle('Новость о Dota 2')
            ->setContent('Что-то про Dota 2')
            ->addCategory($this->getReference('Игры'))
            ->addTag($this->getReference('Dota 2'))
            ->setAuthor($this->getReference('user2'))
            ->setPublishedAt(new \DateTime())
            ->setActive(true);
        $manager->persist($news);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class
        ];
    }
}
