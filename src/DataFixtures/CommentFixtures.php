<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $comment = new Comment();
        $comment
            ->setText('Комментарий бла бла, Путин красавчик')
            ->setUser($this->getReference(UserFixtures::USER_ONE))
            ->setNews($this->getReference('putin-news'));
        $manager->persist($comment);
        $manager->flush();

        $comment = new Comment();
        $comment
            ->setText('Комментарий бла бла, Путин красавчик снова пишу')
            ->setUser($this->getReference(UserFixtures::USER_ONE))
            ->setNews($this->getReference('putin-news'));
        $manager->persist($comment);
        $manager->flush();

        $comment = new Comment();
        $comment
            ->setText('Не соглашусь')
            ->setUser($this->getReference(UserFixtures::USER_TWO))
            ->setNews($this->getReference('putin-news'));
        $manager->persist($comment);
        $manager->flush();

        $comment = new Comment();
        $comment
            ->setText('Bla bla bla')
            ->setUser($this->getReference(UserFixtures::USER_TWO))
            ->setNews($this->getReference('putin-news'))
            ->setActive(false);
        $manager->persist($comment);
        $manager->flush();

        $comment = new Comment();
        $comment
            ->setText('Я фанат футбола')
            ->setUser($this->getReference(UserFixtures::USER_TWO))
            ->setNews($this->getReference('football'));
        $manager->persist($comment);
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            NewsFixtures::class
        ];
    }
}
